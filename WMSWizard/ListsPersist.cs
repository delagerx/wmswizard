﻿using System.Collections.Generic;
using System.Linq;
using WMSWizard.Model;

namespace WMSWizard
{
    public class ListsPersist
    {
        public List<Category> ListCategories { get; set; } = new List<Category>();
        public List<Question> ListQuestions { get; set; } = new List<Question>();
        public List<Answers> ListAnswers { get; set; } = new List<Answers>();
        public int CurrentAnswer { get; set; } = 0;
        public bool ChangeAnswers { get; set; } = false;
        public List<int> ListIdQuestions { get; set; } = new List<int>();
        
        public void ClearLists()
        {
            ListCategories.Clear();
            ListQuestions.Clear();
            ListAnswers.Clear();
            CurrentAnswer = 0;
            ChangeAnswers = false;
            ListIdQuestions.Clear();
        }
    }       
}
