﻿namespace WMSWizard.Model
{
    public class Option
    {
        /// <summary>
        /// Id da opção (Id do registro)
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Valor da opção (Valor do registro)
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// Nome da opção.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Id da próxima questão, se esta opção levar a um desvio.
        /// </summary>
        public int? IdNextQuestion { get; set; }
    }
}