﻿using System.Collections.Generic;
using System.Linq;

namespace WMSWizard.Model
{
    public class Question
    {
        /// <summary>
        /// Id da questão.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Id da questão pai. Para questões do tipo EndLoop, a questão pai é a questão NumberLoop que inicia o loop.
        /// Para questões do tipo EndDeviation, a questão pai é a questão que inicia o desvio. 
        /// Para as demais, a questão pai é a questão cuja resposta influencia a resposta desta.
        /// </summary>
        public int IdParentQuestion { get; set; } = 0;
        /// <summary>
        /// Indíce do loop o qual a questão está na base 0.
        /// </summary>
        public int LoopIndex { get; set; } = 0;
        /// <summary>
        /// Categoria da questão
        /// </summary>
        public Category Category { get; set; }
        /// <summary>
        /// Etapa da questão
        /// </summary>
        public Step Step { get; set; }
        /// <summary>
        /// Título da questão
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Texto de ajuda para a questão
        /// </summary>
        public string HelpText { get; set; }
        /// <summary>
        /// Tipo da questão
        /// </summary>
        public AnswerType AnswerType { get; set; }
        /// <summary>
        /// Tamanho da resposta questão. Para tipo Text, limita o tamanho da string.
        /// </summary>
        public int? AnswerLength { get; set; }
        /// <summary>
        /// Opções pré-definidas para a resposta da questão
        /// </summary>
        public List<Option> Options { get; set; }
        /// <summary>
        /// Id da próxima questão. Necessário somente se o Id da próxima questão não for sequencial.
        /// </summary>
        public int? IdNextQuestion { get; set; }
        /// <summary>
        /// Respostas da questão pai.
        /// </summary>
        public List<Answer> ParentAnswers { get; set; }
        /// <summary>
        /// Lista de respostas.
        /// </summary>
        public List<Answer> ListAnswers { get; set; }
        /// <summary>
        /// Médodo que será chamado ao ser requisitada a resposta armazenada da questão.
        /// </summary>
        public AnswersMethod DbAnswers { get; set; } = null;
        /// <summary>
        /// Método customizado de validação da questão.
        /// </summary>
        public ValidationMethod Validation { get; set; } = null;
        /// <summary>
        /// Método de desvio customizado da questão.
        /// </summary>
        public DeviationMethod Deviation { get; set; } = null;

        public delegate bool ValidationMethod(Question question, Answers answers);
        public delegate int? DeviationMethod(Question question, Answers answers);
        public delegate List<Answer> AnswersMethod(Question thisQuestion);

        internal List<Answer> AnswersMethodDefault (Question thisQuestion)
        {
            return null;
        }

    }
}
