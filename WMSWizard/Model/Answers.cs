﻿using System.Collections.Generic;

namespace WMSWizard.Model
{
    public class Answers
    {
        /// <summary>
        /// Id da resposta gerada automaticamente
        /// </summary>
        public string Id { get; set; } = "";
        /// <summary>
        /// Id da questão
        /// </summary>
        public int IdQuestion { get; set; }
        /// <summary>
        /// Índice do loop da questão.
        /// </summary>
        public int LoopIndex { get; set; }
        /// <summary>
        /// True se resposta é válida e False se contrário.
        /// </summary>
        public bool Valid { get; set; }
        /// <summary>
        /// Lista de respostas.
        /// </summary>
        public List<Answer> ListAnswers { get; set; }

        public bool Equal(Answers other)
        {
            if (ListAnswers.Count != other.ListAnswers.Count)
            {
                return false;
            }
            for (int i = 0; i < ListAnswers.Count; i++)
            {
                if (ListAnswers[i].Value != other.ListAnswers[i].Value)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
