﻿namespace WMSWizard.Model
{
    public class AnswerType
    {
        private AnswerType(string value) { Value = value; }
        public string Value { get; }

        public static AnswerType Text { get { return new AnswerType("Text"); } }
        public static AnswerType Number { get { return new AnswerType("Number"); } }
        public static AnswerType Radio { get { return new AnswerType("Radio"); } }
        public static AnswerType Checkbox { get { return new AnswerType("Checkbox"); } }
        public static AnswerType Select { get { return new AnswerType("Select"); } }
        public static AnswerType TableInput { get { return new AnswerType("TableInput"); } }
        public static AnswerType Action { get { return new AnswerType("Action"); } }
        public static AnswerType InputPage { get { return new AnswerType("InputPage"); } }
        public static AnswerType NumberLoop { get { return new AnswerType("NumberLoop"); } }
        public static AnswerType TableRadio { get { return new AnswerType("TableRadio"); } }
        public static AnswerType EndLoop { get { return new AnswerType("EndLoop"); } }
        public static AnswerType EndDeviation { get { return new AnswerType("EndDeviation"); } }

        public bool Equals(AnswerType answerType)
        {
            return this.Value.Equals(answerType.Value);
        }
    }
}
