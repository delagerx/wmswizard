﻿using System;
using System.Collections.Generic;
using System.Linq;
using WMSWizard.Model;

namespace WMSWizard
{
    public static class WMSWizardController
    {
        public static ListsPersist ListsPersist;

        public static void SetListsPersist(ListsPersist listsPersist)
        {
            ListsPersist = listsPersist;
        }

        public static ListsPersist GetListsPersists()
        {
            if (ListsPersist == null)
            {
                ListsPersist = new ListsPersist();
            }
            return ListsPersist;
        }

        /// <summary>
        /// Adiciona uma resposta à lista de respostas na posição atual.
        /// </summary>
        /// <param name="answers">Resposta a ser adicionada.</param>
        /// <exception cref="QuestionNotFoundException">Questão não encontrada.</exception>
        public static Answers AddAnswers(Answers answers)
        {
            Question currentQuestion = GetQuestion(answers.IdQuestion);
            if (currentQuestion == null && answers.IdQuestion > 0)
            {
                throw new QuestionNotFoundException();
            }
            else if (currentQuestion != null && !currentQuestion.AnswerType.Equals(AnswerType.Action))
            {
                Answers existingAnswers = GetCurrentAnswers();
                if (existingAnswers != null && existingAnswers.IdQuestion == answers.IdQuestion && existingAnswers.LoopIndex == answers.LoopIndex)
                {
                    existingAnswers.Valid = answers.Valid;
                    existingAnswers.ListAnswers = answers.ListAnswers;
                    answers = existingAnswers;
                }
                else
                {
                    answers.Id = ListsPersist.CurrentAnswer.ToString("00") + Guid.NewGuid().ToString();
                    ListsPersist.ListAnswers.Add(answers);
                }
            }
            return answers;
        }

        /// <summary>
        /// Adiciona uma categoria à lista de categorias.
        /// </summary>
        /// <param name="category">Categoria a ser adicionada</param>
        public static void AddCategory(Category category)
        {
            ListsPersist.ListCategories.Add(category);
        }

        /// <summary>
        /// Adiciona uma lista de respostas ao final do fluxo ou substitui uma resposta caso ela já exista para a questão.
        /// </summary>
        /// <param name="listAnswers">Lista de respostas a serem adicionadas.</param>
        public static void AddAnswersRange(List<Answers> listAnswers)
        {
            foreach (Answers answers in listAnswers)
            {
                answers.Valid = ValidateAnswers(answers);
                Answers existAnswers = ListsPersist.ListAnswers.Where(a => a.IdQuestion == answers.IdQuestion && a.LoopIndex == answers.LoopIndex).FirstOrDefault();
                if (existAnswers == null)
                {
                    ListsPersist.ListAnswers.Add(answers);
                }
                else
                {
                    existAnswers.Valid = answers.Valid;
                    existAnswers.ListAnswers = answers.ListAnswers;
                }

            }
            UpdateAnswersId();
        }

        /// <summary>
        /// Adiciona uma questão à lista de questões.
        /// </summary>
        /// <param name="question">Questão a ser adicionada</param>
        public static void AddQuestion(Question question)
        {
            if (question.DbAnswers == null)
            {
                question.DbAnswers = new Question.AnswersMethod(question.AnswersMethodDefault);
            }
            ListsPersist.ListQuestions.Add(question);
        }

        /// <summary>
        /// Verifica se houve mudança na resposta da questão atual.
        /// </summary>
        /// <param name="newAnswer">Nova resposta</param>
        /// <returns>true se as respostas são iguais e false se a resposta não existir ou forem diferentes.</returns>
        public static bool AnswerChange(Answers newAnswer)
        {
            bool answersChange = false;
            Answers currentAnswers = GetCurrentAnswers();
            if (currentAnswers == null)
            {
                return false;
            }
            answersChange = !currentAnswers.Equal(newAnswer);
            ListsPersist.ChangeAnswers |= answersChange;
            return answersChange;
        }

        /// <summary>
        /// Retorna o wizard ao seu estado inicial.
        /// </summary>
        public static void ClearLists()
        {
            ListsPersist.ClearLists();
        }

        /// <summary>
        /// Delete todas as respostas de uma questão.
        /// </summary>
        /// <param name="question">Questão a ter suas respostas retiradas</param>
        public static void DeleteAnswers(Question question)
        {
            List<Answers> listAnswersToDelete = ListsPersist.ListAnswers.Where(a => a.IdQuestion == question.Id).ToList();
            int index;
            foreach (Answers a in listAnswersToDelete)
            {
                index = ListsPersist.ListAnswers.IndexOf(a);
                if (index <= ListsPersist.CurrentAnswer)
                {
                    ListsPersist.CurrentAnswer--;
                }
                ListsPersist.ListAnswers.Remove(a);
            }
        }

        /// <summary>
        /// Zera o LoopIndex dos filhos de uma questão do tipo NumberLoop.
        /// </summary>
        /// <param name="question">Questão do tipo LoopIndex</param>
        private static void EraseLoopIndexChildren(Question question)
        {
            Question endLoop = ListsPersist.ListQuestions.Where(q => q.IdParentQuestion == question.Id && q.AnswerType.Equals(AnswerType.EndLoop)).FirstOrDefault();
            List<int> listIdLoop = ListsPersist.ListQuestions.Where(q => q.Id > question.Id && q.Id < endLoop.Id).Select(q => q.Id).ToList();
            ListsPersist.ListQuestions.Where(q => listIdLoop.Contains(q.Id)).Select(q => { q.LoopIndex = 0; return q; }).ToList();
        }

        /// <summary>
        /// Obtém a resposta da questão que está sendo visitada no fluxo
        /// </summary>
        /// <returns>Resposta da questão ou null se não existir</returns>
        public static Answers GetCurrentAnswers()
        {
            if (ListsPersist.CurrentAnswer < ListsPersist.ListAnswers.Count && ListsPersist.CurrentAnswer >= 0)
            {
                return ListsPersist.ListAnswers[ListsPersist.CurrentAnswer];
            }
            return null;
        }

        public static int GetCurrentPosition()
        {
            return ListsPersist.CurrentAnswer;
        }

        /// <summary>
        /// Obtém a primeira resposta na lista de uma questão.
        /// </summary>
        /// <param name="question">Questão a ser obtida a resposta</param>
        /// <returns>Resposta da questão ou null se não existir</returns>
        public static Answers GetFirstAnswers(Question question)
        {
            return ListsPersist.ListAnswers.Where(x => x.IdQuestion == question.Id && x.LoopIndex == question.LoopIndex).FirstOrDefault();
        }

        /// <summary>
        /// Obtém a primeira questão da lista de questões.
        /// </summary>
        /// <returns>Primeira questão da lista de questões</returns>
        public static Question GetFirstQuestion()
        {
            return ListsPersist.ListQuestions.Where(q => q.Id == ListsPersist.ListQuestions.Min(x => x.Id)).FirstOrDefault();
        }

        /// <summary>
        /// Obtém a última questão respondida do fluxo.
        /// </summary>
        /// <returns></returns>
        public static Question GetLastAnsweredQuestion()
        {
            return GetQuestion(GetLastAnswers().IdQuestion);
        }

        /// <summary>
        /// Obtém a última resposta armazenada na lista de respostas.
        /// </summary>
        /// <returns>Última resposta armazenada</returns>
        public static Answers GetLastAnswers()
        {
            return ListsPersist.ListAnswers.LastOrDefault();
        }

        /// <summary>
        /// Obtém a última resposta da lista de respostas de uma questão até a posição atual.
        /// </summary>
        /// <param name="question">Questão que se quer as últimas respostas.</param>
        /// <returns>Últimas respostas da questão</returns>
        public static Answers GetLastAnswers(Question question)
        {
            return ListsPersist.ListAnswers.GetRange(0, ListsPersist.CurrentAnswer).Where(x => x.IdQuestion == question.Id && x.LoopIndex == question.LoopIndex).LastOrDefault();
        }

        /// <summary>
        /// Obtém a lista de respostas.
        /// </summary>
        /// <returns>Lista de respostas</returns>
        public static List<Answers> GetListAnswers()
        {
            return ListsPersist.ListAnswers;
        }

        /// <summary>
        /// Obtém a lista de respostas.
        /// </summary>
        /// <param name="category">Categoria a se obter as respostas</param>
        /// <returns>Lista de respostas conforme a categoria</returns>
        public static List<Answers> GetListAnswers(int categoryId)
        {
            List<int> listIds = ListsPersist.ListQuestions.Where(q => q.Category.Id == categoryId).Select(q => q.Id).ToList();
            return ListsPersist.ListAnswers.Where(a => listIds.Contains(a.IdQuestion)).ToList();
        }

        /// <summary>
        /// Obtém a lista de categorias.
        /// </summary>
        /// <returns>Lista de categorias</returns>
        public static List<Category> GetListCategories()
        {
            return ListsPersist.ListCategories;
        }

        /// <summary>
        /// Obtém a lista de questões.
        /// </summary>
        /// <returns>Lista de questões</returns>
        public static List<Question> GetListQuestions()
        {
            return ListsPersist.ListQuestions;
        }

        /// <summary>
        /// Obtém a lista de questões da categoria.
        /// </summary>
        /// <returns>Lista de questões</returns>
        public static List<Question> GetListQuestions(int categoryId)
        {
            return ListsPersist.ListQuestions.Where(q => q.Category.Id == categoryId).ToList();
        }

        /// <summary>
        /// Obtém lista das etapas da categoria.
        /// </summary>
        /// <param name="category">Categoria</param>
        /// <returns>Lista das etapas</returns>
        public static List<Step> GetListSteps(int categoryId)
        {
            return ListsPersist.ListQuestions.Where(q => q.Category.Id == categoryId && q.Step != null).Select(q => q.Step).ToList();
        }

        /// <summary>
        /// Obtém a resposta da questão posterior à atual questão do fluxo.
        /// </summary>
        /// <returns>Resposta da questão ou null se não existir</returns>
        public static Answers GetNextAnswers()
        {
            ListsPersist.CurrentAnswer++;
            return GetCurrentAnswers();
        }

        /// <summary>
        /// Obtém a próxima questão com base na respostas da questão atual.
        /// </summary>
        /// <param name="answers">Serialização de um objeto Answers</param>
        /// <returns>Próxima questão</returns>
        /// <exception cref="InvalidAnswerException">Resposta inválida.</exception>
        public static Question GetNextQuestion(Answers answers)
        {
            ValidateAnswers(answers);
            if (answers.Valid)
            {
                bool answersChange = AnswerChange(answers);

                Question currentQuestion = GetQuestion(answers.IdQuestion);
                if (currentQuestion.AnswerType.Equals(AnswerType.NumberLoop) && answersChange && currentQuestion.Deviation == null)
                {
                    ReorganizeLoopAnswers(currentQuestion, answers);
                }

                Answers currentAnswers = AddAnswers(answers);
                Question nextQuestion;
                if (currentAnswers.IdQuestion > 0)
                {
                    nextQuestion = GetNextQuestion(null, currentAnswers, answersChange);
                    if (nextQuestion != null && !nextQuestion.AnswerType.Equals(AnswerType.Action))
                    {
                        nextQuestion.ParentAnswers = nextQuestion.IdParentQuestion > 0 && ListsPersist.ListAnswers.Where(x => x.IdQuestion == nextQuestion.IdParentQuestion && x.LoopIndex == nextQuestion.LoopIndex).Count() > 0 ? ListsPersist.ListAnswers.Where(x => x.IdQuestion == nextQuestion.IdParentQuestion && x.LoopIndex == nextQuestion.LoopIndex).LastOrDefault().ListAnswers : new List<Answer> { new Answer { Id = 0, Value = "" } };
                        Answers nextAnswers = GetNextAnswers();
                        if (nextAnswers != null && nextAnswers.IdQuestion == nextQuestion.Id)
                        {
                            nextQuestion.ListAnswers = nextAnswers.ListAnswers;
                        }
                        else
                        {
                            nextQuestion.ListAnswers = nextQuestion.DbAnswers(nextQuestion);
                            InsertAnswers(nextQuestion);
                        }
                    }
                    return nextQuestion;
                }
                else
                {
                    return GetFirstQuestion();
                }
            }
            else
            {
                throw new InvalidAnswerException();
            }
        }

        /// <summary>
        /// Obtém a próxima questão do fluxo.
        /// </summary>
        /// <param name="currentQuestion">Questão atual</param>
        /// <param name="currentAnswers">Respostas da questão atual</param>
        /// <param name="answersChange">Resposta foi alterada</param>
        /// <returns>Próxima questão</returns>
        private static Question GetNextQuestion(Question currentQuestion, Answers currentAnswers, bool answersChange)
        {
            Question nextQuestion;
            currentQuestion = currentQuestion ?? GetQuestion(currentAnswers.IdQuestion);
            int loopIndex = currentQuestion.AnswerType.Equals(AnswerType.NumberLoop) ? 0 : currentQuestion.LoopIndex;

            int? idNextQuestion = VerifyDeviation(currentQuestion, currentAnswers, answersChange) ?? currentQuestion.IdNextQuestion ?? currentQuestion.Id + 1;

            nextQuestion = GetQuestion(idNextQuestion ?? 0);
            if (nextQuestion != null)
            {
                if (nextQuestion.AnswerType.Equals(AnswerType.EndLoop))
                {
                    int loop = 0;
                    Question parentQuestion = GetQuestion(nextQuestion.IdParentQuestion);
                    int.TryParse(GetFirstAnswers(parentQuestion).ListAnswers.LastOrDefault().Value, out loop);
                    if (nextQuestion.LoopIndex + 1 < loop)
                    {
                        loopIndex = ++nextQuestion.LoopIndex;
                        nextQuestion = GetNextQuestion(parentQuestion, GetLastAnswers(parentQuestion), false);
                        nextQuestion.LoopIndex = loopIndex;
                    }
                    else
                    {
                        nextQuestion = GetNextQuestion(nextQuestion, null, false);
                    }
                }
                else if (nextQuestion.AnswerType.Equals(AnswerType.EndDeviation))
                {
                    nextQuestion.LoopIndex = loopIndex;
                    nextQuestion = GetNextQuestion(nextQuestion, null, false);
                }
                else
                {
                    nextQuestion.LoopIndex = loopIndex;
                }
            }

            return nextQuestion;
        }

        /// <summary>
        /// Obtém a resposta da pergunta especificada em IdParentQuestion
        /// </summary>
        /// <param name="question">Pergunta filha</param>
        /// <returns>Lista das respostas da pergunta pai</returns>
        public static List<Answer> GetParentAnswers(Question question)
        {
            return question.IdParentQuestion > 0 ? GetLastAnswers(GetQuestion(question.IdParentQuestion)).ListAnswers : null;
        }

        /// <summary>
        /// Obtém as respostas da questão anterior à questão atual do fluxo. 
        /// </summary>
        /// <returns>Respostas da questão anterior</returns>
        public static Answers GetPreviousAnswers()
        {
            if (ListsPersist.CurrentAnswer > 0)
            {
                ListsPersist.CurrentAnswer--;
                return GetCurrentAnswers();
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Obtém as respostas da questão com id igual a <paramref name="idQuestion"/> e imediateamente anterior à <paramref name="answers"/>.
        /// </summary>
        /// <param name="idQuestion">Id da questão a ser retornada a resposta.</param>
        /// <param name="answers">Resposta que limite</param>
        /// <returns>Resposta da questão imediatamente anterior.</returns>
        public static Answers GetPreviousAnswers(int idQuestion, Answers answers)
        {
            return ListsPersist.ListAnswers.Where(a => a.IdQuestion == idQuestion && a.Id.CompareTo(answers.Id) == -1).LastOrDefault();
        }

        /// <summary>
        /// Obtém a questão anterior com base na respostas da questão atual.
        /// </summary>
        /// <param name="answers">Serialização de um objeto Answers</param>
        /// <returns>Questão anterior</returns>
        public static Question GetPreviousQuestion(Answers answers)
        {
            Answers previousAnswers;
            if (answers.IdQuestion == 0)
            {
                previousAnswers = GetCurrentAnswers();
            }
            else
            {
                ValidateAnswers(answers);
                AddAnswers(answers);
                previousAnswers = GetPreviousAnswers();
            }

            if (previousAnswers == null)
            {
                return null;
            }
            Question previousQuestion = GetQuestion(previousAnswers.IdQuestion);
            UpdateNumberLoopIndexes(previousAnswers);
            previousQuestion.LoopIndex = previousAnswers.LoopIndex;
            previousQuestion.ListAnswers = previousAnswers.ListAnswers.Count > 0 ? previousAnswers.ListAnswers : previousQuestion.ListAnswers;
            previousQuestion.ParentAnswers = new List<Answer> { new Answer { Id = 0, Value = "" } };
            Question parentQuestion = GetQuestion(previousQuestion.IdParentQuestion);
            if (parentQuestion != null)
            {
                //parentQuestion.LoopIndex = previousQuestion.LoopIndex;
                previousQuestion.ParentAnswers = GetLastAnswers(parentQuestion).ListAnswers;
            }
            return previousQuestion;
        }

        /// <summary>
        /// Obtém a questão de mesmo id
        /// </summary>
        /// <param name="id">Id da questão</param>
        /// <returns>Questão com de mesmo id ou null se não existir</returns>
        public static Question GetQuestion(int id)
        {
            return ListsPersist.ListQuestions.Where(q => q.Id == id).FirstOrDefault();
        }

        /// <summary>
        /// Obtem a questão conforme o id da resposta
        /// </summary>
        /// <param name="answers">Resposta da questão atual que será salva.</param>
        /// <param name="answersId">Id da resposta que se quer a questão</param>
        /// <returns></returns>
        public static Question GetQuestionByAnswersId(Answers answers, string answersId)
        {
            Answers getAnswers = ListsPersist.ListAnswers.Where(a => a.Id.Equals(answersId)).FirstOrDefault();

            Question question = GetQuestion(answers.IdQuestion);
            bool answersChange = AnswerChange(answers);

            ValidateAnswers(answers);
            answers = AddAnswers(answers);
            if (question != null && question.Id > 0)
                VerifyDeviation(question, answers, answersChange);

            if (getAnswers != null && ListsPersist.ListAnswers.Contains(getAnswers))
            {
                answers = getAnswers;
            }
            question = GetQuestion(answers.IdQuestion);
            ListsPersist.CurrentAnswer = ListsPersist.ListAnswers.IndexOf(answers);
            question.LoopIndex = answers.LoopIndex;
            question.ListAnswers = answers != null ? answers.ListAnswers : question.DbAnswers(question);
            if (question.IdParentQuestion > 0)
            {
                Question parentQuestion = GetQuestion(question.IdParentQuestion);
                question.ParentAnswers = ListsPersist.ListAnswers.Where(a => a.IdQuestion == parentQuestion.Id && a.LoopIndex == parentQuestion.LoopIndex).FirstOrDefault().ListAnswers;
            }
            UpdateNumberLoopIndexes(answers);
            return question;
        }

        /// <summary>
        /// Obtém a primeira questão de uma categoria.
        /// </summary>
        /// <param name="answers">Resposta da questão atual que será salva.</param>
        /// <param name="categoryId">Id da categoria que se quer a primeira questão</param>
        /// <returns></returns>
        public static Question GetQuestionByCategory(Answers answers, int categoryId)
        {
            Question question = GetQuestion(answers.IdQuestion);
            bool answersChange = AnswerChange(answers);

            ValidateAnswers(answers);
            AddAnswers(answers);
            if (question != null && question.Id > 0)
                VerifyDeviation(question, answers, answersChange);

            question = ListsPersist.ListQuestions.Where(q => q.Category != null && q.Category.Id == categoryId).OrderBy(q => q.LoopIndex).FirstOrDefault();
            if (question != null)
            {
                answers = ListsPersist.ListAnswers.Where(a => a.IdQuestion == question.Id).FirstOrDefault();
                ListsPersist.CurrentAnswer = ListsPersist.ListAnswers.IndexOf(answers);
                question.LoopIndex = answers.LoopIndex;
                question.ListAnswers = answers != null ? answers.ListAnswers : question.DbAnswers(question);
                if (question.IdParentQuestion > 0)
                {
                    Question parentQuestion = GetQuestion(question.IdParentQuestion);
                    question.ParentAnswers = ListsPersist.ListAnswers.Where(a => a.IdQuestion == parentQuestion.Id && a.LoopIndex == parentQuestion.LoopIndex).FirstOrDefault().ListAnswers;
                }
            }
            UpdateNumberLoopIndexes(answers);
            return question;
        }

        /// <summary>
        /// Obtém a primeira questão de uma etapa.
        /// </summary>
        /// <param name="answers">Resposta da questão atual que será salva.</param>
        /// <param name="stepId">Id da etapa que se quer a primeira questão</param>
        /// <returns></returns>
        public static Question GetQuestionByStep(Answers answers, int stepId)
        {
            Question question = GetQuestion(answers.IdQuestion);
            bool answersChange = AnswerChange(answers);

            ValidateAnswers(answers);
            AddAnswers(answers);
            if (question != null && question.Id > 0)
                VerifyDeviation(question, answers, answersChange);

            question = ListsPersist.ListQuestions.Where(q => q.Step != null && q.Step.Id == stepId).OrderBy(q => q.LoopIndex).FirstOrDefault();
            if (question != null)
            {
                answers = ListsPersist.ListAnswers.Where(a => a.IdQuestion == question.Id).FirstOrDefault();
                ListsPersist.CurrentAnswer = ListsPersist.ListAnswers.IndexOf(answers);
                question.LoopIndex = answers.LoopIndex;
                question.ListAnswers = answers != null ? answers.ListAnswers : question.DbAnswers(question);
                if (question.IdParentQuestion > 0)
                {
                    Question parentQuestion = GetQuestion(question.IdParentQuestion);
                    question.ParentAnswers = ListsPersist.ListAnswers.Where(a => a.IdQuestion == parentQuestion.Id && a.LoopIndex == parentQuestion.LoopIndex).FirstOrDefault().ListAnswers;
                }
            }
            UpdateNumberLoopIndexes(answers);
            return question;
        }

        /// <summary>
        /// Obtém o número de questões.
        /// </summary>
        /// <param name="categoryId">Id da categoria</param>
        /// <returns>Número de questões</returns>
        public static int GetQuestionsCount(int categoryId)
        {
            int questionsCount = GetListQuestions(categoryId).Where(q => !q.AnswerType.Equals(AnswerType.EndLoop) && !q.AnswerType.Equals(AnswerType.EndDeviation) && !q.AnswerType.Equals(AnswerType.Action)).Count();
            int answersCount = GetListAnswers(categoryId).Count;
            int countAux = 0;
            List<int> factors = new List<int>() { 1 };

            foreach (Question question in GetListQuestions(categoryId).Where(q => !q.AnswerType.Equals(AnswerType.EndDeviation) && !q.AnswerType.Equals(AnswerType.Action)))
            {
                if (question.AnswerType.Equals(AnswerType.NumberLoop))
                {
                    countAux += 1 * factors.Last();
                    List<Answer> listAnswer = GetListAnswers(categoryId).Where(a => a.IdQuestion == question.Id).Select(a => a.ListAnswers.FirstOrDefault()).ToList();
                    if (listAnswer != null && listAnswer.Count > 0)
                    {
                        factors.Add(listAnswer.Where(a => a != null).Sum(a => int.Parse(a.Value)));
                    }
                    else
                    {
                        factors.Add(factors.Last());
                    }
                }
                else if (question.AnswerType.Equals(AnswerType.EndLoop))
                {
                    factors.Remove(factors.Last());
                }
                else
                {
                    countAux += 1 * factors.Last();
                }
            }

            return Math.Max(questionsCount, Math.Max(answersCount, countAux));
        }

        /// <summary>
        /// Define a próxima pergunta na inicialização do Wizard.
        /// </summary>
        /// <param name="currentQuestion">Questão atual do fluxo</param>
        /// <param name="currentAnswers">Resposta da questão atual</param>
        /// <returns>Próxima questão do fluxo</returns>
        private static Question InitGetNextQuestion(Question currentQuestion, Answers currentAnswers)
        {
            int loopIndex = currentQuestion.LoopIndex;
            currentQuestion = currentQuestion ?? GetQuestion(currentAnswers.IdQuestion);

            int? idNextQuestion = null;

            if (currentQuestion.Deviation != null)
            {
                idNextQuestion = currentQuestion.Deviation(currentQuestion, currentAnswers);
            }

            if (currentQuestion.Options != null && currentAnswers.ListAnswers.Count > 0)
            {
                var option = currentQuestion.Options.Where(o => o.Value == currentAnswers.ListAnswers.FirstOrDefault().Value).FirstOrDefault();
                idNextQuestion = option == null ? null : option.IdNextQuestion;
            }

            idNextQuestion = idNextQuestion ?? currentQuestion.IdNextQuestion ?? currentQuestion.Id + 1;
            Question nextQuestion = GetQuestion(idNextQuestion ?? 0);
            if (nextQuestion != null)
            {
                nextQuestion.LoopIndex = loopIndex;
                if (nextQuestion.AnswerType.Equals(AnswerType.EndLoop))
                {
                    int loop = 0;
                    Question parentQuestion = GetQuestion(nextQuestion.IdParentQuestion);
                    Answer answer = ListsPersist.ListAnswers.Where(x => x.IdQuestion == parentQuestion.Id && x.LoopIndex == parentQuestion.LoopIndex).FirstOrDefault().ListAnswers.LastOrDefault();
                    if (answer != null) int.TryParse(answer.Value, out loop);
                    if (nextQuestion.LoopIndex + 1 < loop)
                    {
                        loopIndex++;
                        nextQuestion = InitGetNextQuestion(parentQuestion, ListsPersist.ListAnswers.Where(a => a.IdQuestion == parentQuestion.Id && a.LoopIndex == parentQuestion.LoopIndex).FirstOrDefault());
                        nextQuestion.LoopIndex = loopIndex;
                    }
                    else
                    {
                        nextQuestion = InitGetNextQuestion(nextQuestion, null);
                    }
                }
                else if (nextQuestion.AnswerType.Equals(AnswerType.EndDeviation))
                {
                    nextQuestion = InitGetNextQuestion(nextQuestion, null);
                }
            }
            if (nextQuestion != null && nextQuestion.IdParentQuestion > 0)
            {
                nextQuestion.ParentAnswers = ListsPersist.ListAnswers.Where(x => x.IdQuestion == nextQuestion.IdParentQuestion).LastOrDefault().ListAnswers;
            }
            return nextQuestion;
        }

        /// <summary>
        /// Método para criar a lista de respostas conforme o retorno da função DbAnswers de cada questão.
        /// </summary>
        public static void InitializeAnswers()
        {
            Answers answers;
            Question question = GetFirstQuestion();
            while (question != null)
            {
                answers = new Answers()
                {
                    IdQuestion = question.Id,
                    LoopIndex = question.LoopIndex
                };
                try
                {
                    answers.ListAnswers = question.DbAnswers(question) ?? new List<Answer> { new Answer() { Id = 0, Value = "" } };
                }
                catch
                {
                    answers.ListAnswers = new List<Answer> { new Answer() { Id = 0, Value = "" } };
                }
                try
                {
                    if (question.Validation != null)
                    {
                        answers.Valid = question.Validation(question, answers);
                    }
                    else
                    {
                        answers.Valid = ValidationDefault(question, answers.ListAnswers);
                    }
                }
                catch
                {
                    answers.Valid = false;
                }

                answers.LoopIndex = question.LoopIndex;
                question.ListAnswers = answers.ListAnswers;
                AddAnswers(answers);
                ListsPersist.CurrentAnswer++;
                question = InitGetNextQuestion(question, answers);
            }
            ListsPersist.CurrentAnswer = 0;
            ValidateAllAnswers();
            ListsPersist.ListQuestions.Select(q => { q.LoopIndex = 0; return q; }).ToList();
            UpdateAnswersId();
        }

        /// <summary>
        /// Determina se a lista de respostas foi alterada.
        /// </summary>
        /// <returns>Verdadeiro se foi alterada ou falso se contrário</returns>
        public static bool IsAnswwersChange()
        {
            return ListsPersist.ChangeAnswers;
        }

        /// <summary>
        /// Insere uma resposta na posição atual na lista de respostas.
        /// </summary>
        /// <param name="answers">Resposta a ser inserida</param>
        public static void InsertAnswers(Answers answers)
        {
            answers.Id = ListsPersist.CurrentAnswer.ToString("00") + Guid.NewGuid().ToString();
            ListsPersist.ListAnswers.Insert(ListsPersist.CurrentAnswer, answers);
            if (ListsPersist.CurrentAnswer < ListsPersist.ListAnswers.Count)
            {
                UpdateAnswersId();
            }
            ListsPersist.ChangeAnswers = true;
        }

        /// <summary>
        /// Insere uma resposta na posição especificada. 
        /// Se a posição for maior que o tamanho da lista, a <paramref name="answers"/> será adicionada ao final.
        /// </summary>
        /// <param name="position">Posição a ser inserida a <paramref name="answers"/></param>
        /// <param name="answers">Resposta a ser inserida</param>
        public static void InsertAnswers(int position, Answers answers)
        {
            if (position >= ListsPersist.ListAnswers.Count)
            {
                answers.Id = ListsPersist.ListAnswers.Count.ToString("00") + Guid.NewGuid().ToString();
                ListsPersist.ListAnswers.Add(answers);
            }
            else
            {
                answers.Id = position.ToString("00") + Guid.NewGuid().ToString();
                ListsPersist.ListAnswers.Insert(position, answers);
            }
            ListsPersist.ChangeAnswers = true;
        }

        /// <summary>
        /// Insere uma resposta na posição atual na lista de respostas.
        /// </summary>
        /// <param name="answers">Resposta a ser inserida</param>
        private static void InsertAnswers(Question question)
        {
            Answers answers = new Answers()
            {
                IdQuestion = question.Id,
                LoopIndex = question.LoopIndex,
                Valid = true,
                ListAnswers = question.ListAnswers
            };
            InsertAnswers(answers);
        }

        /// <summary>
        /// Reorganiza a lista de respostas quando há alteração na resposta de pergunta com desvio.
        /// </summary>
        /// <param name="deviationQuestion">Questão com o desvio.</param>
        public static void ReorganizeDeviationAnswers(Question deviationQuestion)
        {
            Question endDeviation = ListsPersist.ListQuestions.Where(q => q.IdParentQuestion == deviationQuestion.Id && q.AnswerType.Equals(AnswerType.EndDeviation)).FirstOrDefault();
            ListsPersist.ListIdQuestions = ListsPersist.ListQuestions.Where(q => q.Id > deviationQuestion.Id && q.Id < endDeviation.Id).Select(q => q.Id).ToList();

            while (ListsPersist.CurrentAnswer + 1 < ListsPersist.ListAnswers.Count() && ListsPersist.ListIdQuestions.Contains(ListsPersist.ListAnswers[ListsPersist.CurrentAnswer + 1].IdQuestion))
            {
                ListsPersist.ListAnswers.RemoveAt(ListsPersist.CurrentAnswer + 1);
                ListsPersist.ChangeAnswers = true;
            }
        }

        /// <summary>
        /// Reorganiza a lista de respostas quando há alteração na quantidade de respostas de um loop de perguntas
        /// </summary>
        /// <param name="loopQuestion">Questão do tipo NumberLoop.</param>
        /// <param name="answers">Nova resposta com o número de loops a serem feitos.</param>
        public static void ReorganizeLoopAnswers(Question loopQuestion, Answers answers)
        {
            Question endLoop = ListsPersist.ListQuestions.Where(q => q.IdParentQuestion == loopQuestion.Id && q.AnswerType.Equals(AnswerType.EndLoop)).FirstOrDefault();
            ListsPersist.ListIdQuestions = ListsPersist.ListQuestions.Where(q => q.Id > loopQuestion.Id && q.Id < endLoop.Id).Select(q => q.Id).ToList();
            int currentValue = int.Parse(loopQuestion.ListAnswers[0].Value);
            int answerValue = int.Parse(answers.ListAnswers[0].Value);

            if (answerValue < currentValue)
            {
                int auxPointer = ListsPersist.CurrentAnswer + 1;

                //busca local dos loops relativos à questão
                while (ListsPersist.ListIdQuestions.Contains(ListsPersist.ListAnswers[auxPointer].IdQuestion) && ListsPersist.ListAnswers[auxPointer].LoopIndex <= answerValue - 1)
                {
                    auxPointer++;
                }

                while (auxPointer < ListsPersist.ListAnswers.Count() && ListsPersist.ListIdQuestions.Contains(ListsPersist.ListAnswers[auxPointer].IdQuestion))
                {
                    ListsPersist.ListAnswers.RemoveAt(auxPointer);
                    ListsPersist.ChangeAnswers = true;
                }
            }
        }

        /// <summary>
        /// Posiciona a referência de resposta na última resposta da categoria.
        /// </summary>
        /// <param name="categoryId">Id da categoria</param>
        public static void SetCurrentAnswersHasLast(int categoryId)
        {
            List<int> listIds = ListsPersist.ListQuestions.Where(q => q.Category.Id == categoryId).Select(q => q.Id).ToList();
            Answers answers = ListsPersist.ListAnswers.Where(a => listIds.Contains(a.IdQuestion)).LastOrDefault();
            ListsPersist.CurrentAnswer = ListsPersist.ListAnswers.IndexOf(answers);
        }

        /// <summary>
        /// Valida a resposta de uma questão questão
        /// </summary>
        /// <param name="answers">Resposta</param>
        /// <param name="errorMsg">Mensagem de retorno do erro</param>
        /// <returns></returns>
        public static bool ValidateAnswers(Answers answers)
        {
            Question question = GetQuestion(answers.IdQuestion);
            try
            {
                if (question.Validation != null)
                {
                    answers.Valid = question.Validation(question, answers);
                }
                else
                {
                    answers.Valid = ValidationDefault(question, answers.ListAnswers);
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Método de validação padrão.
        /// </summary>
        /// <param name="question">Questão</param>
        /// <param name="answer">Respostas</param>
        private static bool ValidationDefault(Question question, List<Answer> answer)
        {
            int i;
            bool valid = true;
            if (answer.Count == 0)
            {
                valid = false;
            }
            else if (question.AnswerType.Equals(AnswerType.Radio))
            {
                valid = !string.IsNullOrEmpty(answer.FirstOrDefault().Value);
            }
            else if (question.AnswerType.Equals(AnswerType.Select))
            {
                valid = !answer.LastOrDefault().Value.Equals("-1");
            }
            else if (question.AnswerType.Equals(AnswerType.TableRadio))
            {
                valid = answer.Where(x => x.Id == 0 && !x.Value.Equals("")).Count() > 0;
            }
            else if (question.AnswerType.Equals(AnswerType.Number) || question.AnswerType.Equals(AnswerType.NumberLoop))
            {
                valid = int.TryParse(answer.FirstOrDefault().Value, out i) ? int.Parse(answer.LastOrDefault().Value) >= question.AnswerLength : false;
            }
            return valid;
        }

        /// <summary>
        /// Verifica se haverá desvio na sequência do fluxo com base na resposta da questão.
        /// </summary>
        /// <param name="question">Questão da resposta</param>
        /// <param name="answers">Resposta da questão</param>
        /// <param name="answersChange">Houve alteração na resposta que estava armazenada</param>
        /// <returns></returns>
        private static int? VerifyDeviation(Question question, Answers answers, bool answersChange)
        {
            int? idNextQuestion = null;
            if (question.Deviation != null)
            {
                idNextQuestion = question.Deviation(question, answers);
            }
            else if (question.Options != null && answers.ListAnswers.Count > 0)
            {
                idNextQuestion = question.Options.Where(o => o.Value.Equals(answers.ListAnswers.FirstOrDefault().Value)).FirstOrDefault().IdNextQuestion;
                if (idNextQuestion > 0 && answersChange)
                {
                    ReorganizeDeviationAnswers(question);
                }
            }
            return idNextQuestion;
        }

        /// <summary>
        /// Verifica o percentual de respostas válidas e limpa a lista de respostas caso este percentual não é aceito.
        /// </summary>
        private static void ValidateAllAnswers()
        {
            int countAnswers = ListsPersist.ListAnswers.Count();
            if ((float)ListsPersist.ListAnswers.Where(a => a.Valid).Count() / countAnswers < 0.5)
            {
                ListsPersist.ListAnswers.Clear();
            }
        }

        /// <summary>
        /// Atualiza os ids das respostas
        /// </summary>
        private static void UpdateAnswersId()
        {
            Guid guid;
            for (int i = ListsPersist.CurrentAnswer; i < ListsPersist.ListAnswers.Count; i++)
            {
                guid = Guid.NewGuid();
                ListsPersist.ListAnswers[i].Id = i.ToString("00") + guid.ToString();
            }
        }

        /// <summary>
        /// Atualiza os loopIndex das respostas irmãs.
        /// </summary>
        /// <param name="answers">Resposta de referência</param>
        private static void UpdateNumberLoopIndexes(Answers answers)
        {
            List<int> listIds = new List<int>();
            int index = ListsPersist.ListAnswers.IndexOf(answers);
            int loop = 0;

            for (int i = index; i >= 0; i--)
            {
                Question _question;
                Answers _answers = ListsPersist.ListAnswers.ElementAt(i);
                if (!listIds.Contains(_answers.IdQuestion))
                {
                    listIds.Add(_answers.IdQuestion);
                    _question = GetQuestion(_answers.IdQuestion);
                    _question.LoopIndex = _answers.LoopIndex;

                    if (_question.AnswerType.Equals(AnswerType.NumberLoop))
                    {
                        Question endLoop = ListsPersist.ListQuestions.Where(q => q.IdParentQuestion == _question.Id && q.AnswerType.Equals(AnswerType.EndLoop)).FirstOrDefault();
                        try
                        {
                            endLoop.LoopIndex = loop;
                        }
                        catch
                        {
                            endLoop.LoopIndex = 0;
                        }
                    }
                    else
                    {
                        loop = _answers.LoopIndex;
                    }
                }
            }
        }
    }
}
