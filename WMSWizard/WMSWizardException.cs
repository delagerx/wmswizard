﻿using System;

namespace WMSWizard
{
    public class QuestionNotFoundException : Exception
    {
        public QuestionNotFoundException()
        {
        }        
    }

    public class InvalidAnswerException : Exception
    {
        public InvalidAnswerException()
        {
        }
    }
}
